package.cpath = package.cpath .. (";/home/fj/c/farm/obj/libfarm.so")
local farm = require( "libfarm" )

farm.insert(1,"Um",1.11)
farm.insert(2,"Dois",22.22)

local fl = require( "fltk4lua" )
local window = fl.Window( 800, 600 )
fl.get_system_colors()
window.color = 12

local menubar = fl.Menu_Bar( 0, 0, 800, 24 )

local farm_win = fl.Window( 8, 40, 480, 368 )
do
	local button_back	= fl.Button( 4, 4, 32, 32, "@<-" )
	local button_refresh 	= fl.Button( 40, 4, 32, 32, "@refresh" )
	local button_insert 	= fl.Button{ 76, 4, 32, 32, "@filesave" }
	
	local tabs = fl.Tabs{ 4, 40, 470, 296, }
	do
		local one = fl.Group{ 8, 60, 470, 296, "Tab&1", }
		do
			local list1		= fl.Browser( 12, 90, 454, 238 )
			
			function button_refresh:callback()
				list1:clear()
				for f in farm.list() do
					list1:add(string.format( "%d\t %s", f:id(), f:name()))
				end
			end
		end
		one:end_group()
		fl.Group.current().resizable = one
	end
	do
		local two = fl.Group{ 8, 60, 470, 296, "Tab&2", }
		do
			local input_id 			= fl.Input{ 16, 96, 60, 24, "ID", type="FL_INT_INPUT" }
			input_id.align 			= fl.ALIGN_TOP + fl.ALIGN_LEFT
			input_id.maximum_size 	= 4
			local input_name 		= fl.Input{ 16, 144, 360, 24, "Name" }
			input_name.align 		= fl.ALIGN_TOP + fl.ALIGN_LEFT
			input_name.maximum_size = 40
			local input_size 		= fl.Spinner{ 16, 192, 120, 24, "Size", type="FL_FLOAT_INPUT",
										minimum=0, maximum=999, step=0.01, value=0.00, }
			input_size.align 		= fl.ALIGN_TOP + fl.ALIGN_LEFT
			
			function button_insert:callback()
				
				farm.insert( input_id.value, input_name.value, input_size.value )
				clear_form()
			end
			
			function clear_form()
				input_id.value 		= ""
				input_name.value 	= ""
				input_size.value 	= "0.00"
			end
		end
		two:hide()
		two:end_group()
	end
	
	function button_back:callback()
		farm_win:hide()
	end
end
farm_win:hide()
farm_win:end_group()

local function open_farm_win()
	farm_win:show()
end
local function quit()
	window:hide()
end
menubar:add( "&File/&Farm", "^f", open_farm_win )
menubar:add( "&File/&Quit", "^q", quit )
window:end_group()

window:show( arg )
fl.run()