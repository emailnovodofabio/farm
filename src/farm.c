#include <stdio.h>
#include <stdlib.h>
#include <wchar.h>
#include <inttypes.h>
#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"

#define FARM_NAME_MAX 50

typedef struct	farm	FARM;
typedef 		FARM*	FARM_PTR;

struct			farm {
	ptrdiff_t	_id;
	wchar_t		_name[FARM_NAME_MAX];
	double		_size;
	
	FARM_PTR	_next;
};

FARM_PTR			head = NULL;
FARM_PTR			aux = NULL;

int length() {
	int			len = 0;
	FARM_PTR	current;
	
	for(current = head; current != NULL; current = current->_next) {
		len++;
	}
	
	return len;
}

int insert_farm ( lua_State* L )
{
	FARM_PTR		new_ptr = malloc (sizeof (FARM));
	
	new_ptr->_id = luaL_checkinteger (L, -3);
	const char *buffer = lua_tostring(L, -2);
	mbstowcs (new_ptr->_name, buffer, FARM_NAME_MAX);
	new_ptr->_size = luaL_checknumber (L, -1);
	new_ptr->_next = head;
	head = new_ptr;
	aux = head;
	printf("C_insert id: %td name: %ls size: %.2f\n", new_ptr->_id, new_ptr->_name, new_ptr->_size);
	return 1;
}

static const char* farm_metatable = "farm";
static int count = 0;

int farm_iter( lua_State* L )
{
	//printf("C_count = %d\n", count+1);
	if (++count > length()) {
		count = 0;
		aux = head;
		return 0;
	}
		
	FARM_PTR f = ( FARM_PTR )lua_newuserdata( L, sizeof( FARM ) );
	
	if (aux != NULL) {
		f->_id = aux->_id;
		wcscpy(f->_name, aux->_name);
		f->_size = aux->_size;
		aux = aux->_next;
	}

	luaL_getmetatable( L, farm_metatable );
	lua_setmetatable( L, -2 );
	
	return 1;
}

int farm_list( lua_State* L )
{
   lua_pushcclosure( L, farm_iter, 1 );
   return 1;
}

int farm_id( lua_State* L )
{
   FARM_PTR f = ( FARM_PTR )luaL_checkudata( L, 1, farm_metatable );
   lua_pushinteger( L, f->_id );
   return 1;
}

int farm_name( lua_State* L )
{
	FARM_PTR f = ( FARM_PTR )luaL_checkudata( L, 1, farm_metatable );
	char buffer[FARM_NAME_MAX];
	wcstombs(buffer, f->_name, FARM_NAME_MAX);
	lua_pushstring( L, buffer );
	return 1;
}

int farm_size( lua_State* L )
{
   FARM_PTR f = ( FARM_PTR )luaL_checkudata( L, 1, farm_metatable );
   lua_pushnumber( L, f->_size );
   return 1;
}

int farm_close( lua_State* L ) { return 0; }

static const struct luaL_Reg farm_methods[] = {
	{ "id", farm_id },
    { "name", farm_name },
	{ "size", farm_size },
	{ "__gc", farm_close },
    { NULL, NULL }
};

static const struct luaL_Reg libfarm[] = { 
	{ "insert", insert_farm },
	{ "list", farm_list },
	{ NULL, NULL }
};

int luaopen_libfarm(lua_State *L) {
	luaL_newmetatable( L, farm_metatable );
	luaL_setfuncs( L, farm_methods, 0 );
	lua_pushvalue(L, -1);
	lua_setfield( L, -2, "__index");
	
    luaL_newlib(L, libfarm);
    return 1;
}